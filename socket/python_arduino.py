import serial
import time


def comunicacionCMD():
    arduino = serial.Serial('/dev/ttyACM0', baudrate=9600, timeout=1.0)
    # Reset arduino totalmente, tener cuidado con esto
    arduino.setDTR(False)
    time.sleep(1)
    arduino.flushInput()
    arduino.setDTR(True)

    print("Starting!")

    while True:
        flag = False
        comando = input('Introduce un comando: ')  # Input
        arduino.write(str.encode(comando))  # Mandar un comando hacia Arduino
        while True:
            # leemos hasta que encontarmos el final de linea
            respuesta = arduino.readline()
            # Mostramos el valor leido y eliminamos el salto de linea del final
            convercion = str(respuesta).replace("b", "")
            if convercion != "''":
                convercion = convercion.replace("'", "")
                convercion = convercion[0:len(convercion)-4]
                print("Valor Arduino: " + convercion)
            if convercion == "''" and flag == True:
                break
            else:
                flag = True
        if comando == "exit":
            break
    arduino.close()  # Finalizamos la comunicacion

#! configuracion de conexion entre maquina y arduino
arduino = serial.Serial('/dev/ttyACM0', baudrate=9600, timeout=1.0)
def sendComand(comando):
    flag = False
    respuesta = "norespuesta"
    arduino.write(str.encode(comando))
    while True:
        # leemos hasta que encontarmos el final de linea
        respuesta = arduino.readline()
        # Mostramos el valor leido y eliminamos el salto de linea del final
        convercion = str(respuesta).rstrip('\n').replace("b", "")
        if convercion != "''" and convercion != "b''":
            convercion = convercion.replace("'", "")
            convercion = convercion[0:len(convercion)-4]
            respuesta = convercion
            break
        if (convercion == "''" or convercion == "b''") and flag:
            break
        else:
            flag = True
    return respuesta